/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'Vent',
    'loader',
    'views/pageLayout',
], function ( $, _, Backbone, Vent, vent, PageLayout) {
    'use strict';

    var AboutUs = PageLayout.extend({

        name: 'about_us',

        initialize: function(options) {
            console.log('HELLO!!');
            console.log(this);
            window.globThis = this;
            debugger;
            alert(123);
            options = options || {};
            this.html = options.html;
            this.sasl = options.sasl;
            this.on('show', this.onShow, this);
        },

        renderData: function () {
            return {html: this.html};
        },

        onShow:  function() {
            this.addEvents({
                'click .back': 'triggerRestaurantView',
            });
        },

        triggerRestaurantView: function() {
            Vent.trigger( 'viewChange', 'restaurant', this.sasl.getUrlKey());
        },

    });

    return AboutUs;
});
