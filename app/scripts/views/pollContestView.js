/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'Vent',
    'loader',
    'views/pageLayout',
    'views/listView',
    'views/partials/pollOptionView',
    'views/partials/prizeView',
    'actions/contestActions',
    'globalHelpers'
], function ($, _, Backbone, Vent, loader, PageLayout, ListView, PollOptionView, PrizeView, contestActions, h) {
    'use strict';

    return PageLayout.extend({

        name: 'pollContest',

        renderData: function () {
            return $.extend(this.model, {
                activationDate: h().toPrettyTime(this.model.activationDate),
                expirationDate: h().toPrettyTime(this.model.expirationDate)
            });
        },

        initialize: function(options) {
            options = options || {};
            this.sasl = options.sasl;
            this.on('show', this.onShow, this);
        },

        onShow: function(){
            this.addEvents({
                'click .back': 'triggerContestsView',
            });
            this.renderOptions();
            this.renderPrizes();
        },

        triggerContestsView: function() {
            Vent.trigger('viewChange', 'contests', this.sasl.getUrlKey(), { reverse: true });
        },

        renderPrizes: function () {
            this.$('.cmntyex_prizes_placeholder').html(
                new ListView({
                    ItemView: PrizeView,
                    collection: new Backbone.Collection(this.model.prizes),
                    update: false,
                    dataRole: 'none',
                    parent: this
                }).render().el
            );
        },

        renderOptions: function () {
            this.$('.cmntyex_options_placeholder').html(
                new ListView({
                    ItemView: PollOptionView,
                    ItemViewOptions: {
                        onClick: this.onPollClick.bind(this)
                    },
                    collection: new Backbone.Collection(this.model.choices),
                    update: false,
                    dataRole: 'none',
                    parent: this
                }).render().el
            );
        },

        onPollClick: function(model) {
            this.withLogIn(function () {
                loader.show("");
                contestActions.enterPoll(
                    this.sasl.sa(),
                    this.sasl.sl(),
                    model.get('contestUUID'),
                    model.get('choiceId')
                ).then(function () {
                    loader.showFlashMessage('Poll entered');
                }, function (e) {
                    loader.showErrorMessage(e, 'error entering poll');
                });
            }.bind(this));
        }

    });

});
