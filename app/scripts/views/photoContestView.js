/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'Vent',
    'loader',
    'viewFactory',
    'views/pageLayout',
    'views/listView',
    'views/partials/prizeView',
    'actions/contestActions',
    'globalHelpers'
], function ($, _, Backbone, Vent, loader, viewFactory, PageLayout, ListView, PrizeView, contestActions, h) {
    'use strict';

    return PageLayout.extend({

        name: 'photoContest',

        renderData: function () {
            return $.extend(this.model, {
                activationDate: h().toPrettyTime(this.model.activationDate),
                expirationDate: h().toPrettyTime(this.model.expirationDate)
            });
        },

        initialize: function(options) {
            options = options || {};
            this.sasl = options.sasl;
            this.on('show', this.onShow, this);
        },

        onShow: function(){
            this.addEvents({
                'click .back': 'triggerContestsView',
                'click .enter_button': 'enterContest'
            });
            this.renderPrizes();
        },

        triggerContestsView: function() {
            Vent.trigger('viewChange', 'contests', this.sasl.getUrlKey(), { reverse: true });
        },

        renderPrizes: function () {
            this.$('.cmntyex_prizes_placeholder').html(
                new ListView({
                    ItemView: PrizeView,
                    collection: new Backbone.Collection(this.model.prizes),
                    update: false,
                    dataRole: 'none',
                    parent: this
                }).render().el
            );
        },

        enterContest: function () {
            this.withLogIn(function () {
                this.openSubview('upload', this.sasl, {
                    action: function (sa, sl, file) {
                        loader.show('');
                        return contestActions.enterPhotoContest(sa, sl, this.model.contestUUID, file)
                            .then(function () {
                                loader.showFlashMessage('contest entered');
                            }, function (e) {
                                loader.showErrorMessage(e, 'error uploading photo');
                            });
                    }.bind(this)
                });
            }.bind(this));
        }

    });

});
