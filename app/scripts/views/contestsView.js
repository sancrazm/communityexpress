/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'Vent',
    'loader',
    'viewFactory',
    'views/pageLayout',
    'views/listView',
    'views/partials/contestItemView',
    'globalHelpers'
], function ($, _, Backbone, Vent, loader, viewFactory, PageLayout, ListView, ContestView) {
    'use strict';

    var ContestsView = PageLayout.extend({

        name: 'contests',

        initialize: function(options) {
            options = options || {};
            this.sasl = options.sasl;
            this.contests = options.contests;
            this.on('show', this.onShow, this);
        },

        onShow: function(){
            this.addEvents({
                'click .back': 'triggerRestaurantView',
            });
            this.renderContests();
        },

        renderContests: function() {
            this.$('.cmntyex-contests_placeholder').html(new ListView({
                ItemView: ContestView,
                ItemViewOptions: {
                    onClick: this.goToContest.bind(this)
                },
                className: 'ui-listview',
                collection: this.contests,
                update: false,
                parent: this
            }).render().el);
        },

        goToContest: function (type, id) {
            switch (type) {
                case 'PHOTO_CONTEST':
                    Vent.trigger('viewChange', 'photoContest', {
                        sasl: this.sasl.getUrlKey(),
                        id:id
                    });
                    break;
                case 'POLL_CONTEST':
                    Vent.trigger('viewChange', 'pollContest', {
                        sasl: this.sasl.getUrlKey(),
                        id:id
                    });
                    break;
                case 'CHECKIN_CONTEST':
                    Vent.trigger('viewChange', 'checkinContest', {
                        sasl: this.sasl.getUrlKey(),
                        id:id
                    });
                    break;
            }
        },

        triggerRestaurantView: function() {
            Vent.trigger('viewChange', 'restaurant', this.sasl.getUrlKey(), { reverse: true });
        },

    });

    return ContestsView;
});
