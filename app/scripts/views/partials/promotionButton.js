/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'loader',
    'controllers/promotionsController'
], function ($, _, Backbone, JST, loader, promotionsController) {
    'use strict';

    var PromotionButton = Backbone.View.extend({

        template: JST['app/scripts/templates/partials/promotionButton.ejs'],

        events: {
            'click': 'open'
        },

        initialize: function (options) {
            options = options || {};
            this.parent = options.parent;

            this.listenTo(this.parent, 'hide', this.remove, this);
        },

        render: function () {
            this.$el.html(this.template());
            return this;
        },

        open: function(pid) {
            loader.show('retrieving promotions');
            promotionsController.fetchPromotionUUIDsBySasl(
                this.model.sa(),
                this.model.sl(),
                this.parent.user.getUID()
            ).then(function(promotions) {
                if(promotions.length < 1) {
                    loader.showFlashMessage('No promotions were found');
                } else {
                    this.parent.openSubview('promotions', promotions, {pid: pid, sasl: this.model});
                }
            }.bind(this), function () {
                loader.showFlashMessage('error retrieving promotions');
            });
        }

    });

    return PromotionButton;
});
