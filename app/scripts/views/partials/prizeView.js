/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'globalHelpers',
    'templates',
    'appConfig',
    'Vent',
    'actions/saslActions'
], function ($, _, Backbone, h, JST, config, Vent, saslActions) {
    'use strict';

    return Backbone.View.extend({

        tagName: 'li',

        template: JST['app/scripts/templates/partials/prize.ejs'],

        events: {
            'click .choose_button': 'choose'
        },

        render: function () {
            this.$el.html(this.template(this.model.attributes));
            return this;
        },

    });

});
