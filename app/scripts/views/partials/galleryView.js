/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var GalleryviewView = Backbone.View.extend({

        template: JST['app/scripts/templates/partials/galleryView.ejs'],

        render: function() {
            this.el.innerHTML = this.template({collection: this.collection});
            return this;
        }
    });

    return GalleryviewView;
});
