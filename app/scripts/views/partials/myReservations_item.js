/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'Vent',
    'globalHelpers'
], function ($, _, Backbone, JST, Vent, h) {
    'use strict';

    var MymessageItemView = Backbone.View.extend({

        template: JST['app/scripts/templates/partials/myReservations_item.ejs'],

        tagName: 'li',

        initialize: function(options) {
            this.parent = options.parent;
            this.$el.attr('data-icon','edit');
        },

        events: {
            'click a': 'modifyReservation'
        },

        modifyReservation: function() {
            this.parent.trigger( 'modify_reservation', this.model );
        },

        _getTrueTime: function() {
            var d = this.model.get('reservationDate'),
                ho = this.model.get('timeRange').startClock.hour,
                m = this.model.get('timeRange').startClock.minute;

            return new Date( d + 'T' + h().toTwoDigit(ho) + ':' + h().toTwoDigit(m) );
        },

        render: function() {
            var date = h().dateToPrettyTime( this._getTrueTime() );
            this.$el.html(this.template( _.extend( this.model.toJSON(), { date: date }) ));
            return this;
        }
    });

    return MymessageItemView;
});
