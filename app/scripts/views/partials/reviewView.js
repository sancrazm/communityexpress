/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'globalHelpers',
    'Vent'
], function ($, _, Backbone, JST, h, Vent ) {
    'use strict';

    var ReviewView = Backbone.View.extend({

        tagName: 'li',

        className: 'cmntyex-review',

        template: JST['app/scripts/templates/partials/review.ejs'],

        render: function() {
            this.$el.html(this.template(this.model.attributes));
            return this;
        }
    });

    return ReviewView;
});
