/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'globalHelpers',
    'Vent'
], function ($, _, Backbone, JST, h, Vent ) {
    'use strict';

    var FavoriteView = Backbone.View.extend({

        tagName: 'li',

        template: JST['app/scripts/templates/partials/favorite.ejs'],

        events: {
            'click a': 'goToRestaurant'
        },

        goToRestaurant: function() {
            var sa = this.model.attributes.saslPair.sa;
            var sl = this.model.attributes.saslPair.sl;
            Vent.trigger('viewChange', 'restaurant', [sa,sl] );
        },

        render: function() {
            var data = h().toViewModel( _.extend(this.model.toJSON(), {
                notifications: this._sumOfNotifications(),
                reservations: this.model.get('reservationWithSASLCount')
            }));
            this.$el.html(this.template(data));
            return this;
        },

        _sumOfNotifications: function() {
            var notes = [
                this.model.get('messageFromSASLCount'),
                this.model.get('notificationsFromSASLCount'),
                this.model.get('responsesFromSASLCount'),
                this.model.get('requestsFromSASLCount')
            ];
            var sum = 0;
            _(notes).each(function(note){
                sum += parseInt(note, 10);
            });
            return sum;
        }
    });

    return FavoriteView;
});
