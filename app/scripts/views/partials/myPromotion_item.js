/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var MypromotionItemView = Backbone.View.extend({
        template: JST['app/scripts/templates/partials/myPromotion_item.ejs']
    });

    return MypromotionItemView;
});
