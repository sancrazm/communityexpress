/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'globalHelpers',
    'Vent'
], function ($, _, Backbone, JST, h, Vent ) {
    'use strict';

    var catalogItem = Backbone.View.extend({

        tagName: 'li',

        className: 'cmntyex-catalog-item',

        template: JST['app/scripts/templates/partials/catalog-item.ejs'],

        events: {
            'click': 'onClick'
        },

        initialize: function (options) {
            this.onClick = function () {
                options.onClick(this.model);
            }.bind(this);
            this.color = options.color;
        },

        render: function() {
            this.$el.html(this.template(_.extend({}, this.model.attributes, {
                color: this.color
            })));
            return this;
        }
    });

    return catalogItem;
});
