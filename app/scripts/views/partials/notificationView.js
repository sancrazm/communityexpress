/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'Geolocation',
    'Vent'
], function ($, _, Backbone, JST, Geolocation, Vent, RestaurantModel ) {
    'use strict';

    var NotificationView = Backbone.View.extend({

        template: JST['app/scripts/templates/partials/notification.ejs'],

        tagName: 'li',

        render: function() {
            this.$el.html(this.template(this.model.attributes));
            return this;
        }
    });

    return NotificationView;

});
