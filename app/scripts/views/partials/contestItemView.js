/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
], function ($, _, Backbone, JST) {
    'use strict';

    var ContestItemView = Backbone.View.extend({

        template: JST['app/scripts/templates/partials/contestItem.ejs'],

        tagName: 'li',

        events: {
            'click': 'clicked'
        },

        initialize: function (options) {
            this.onClick = options.onClick;
        },

        clicked: function () {
            this.onClick(this.model.get('contestType').enumText, this.model.get('contestUUID'));
        },

        render: function() {
            this.$el.html(this.template(this.model.attributes));
            return this;
        }

    });

    return ContestItemView;
});
