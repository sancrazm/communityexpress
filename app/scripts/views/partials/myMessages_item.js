/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'Geolocation',
    'Vent',
    'models/restaurantModel'
], function ($, _, Backbone, JST, Geolocation, Vent, RestaurantModel ) {
    'use strict';

    var MymessageItemView = Backbone.View.extend({

        template: JST['app/scripts/templates/partials/myMessages_item.ejs'],

        tagName: 'li',

        events: {
            'click a': 'goToRestaurant'
        },

        goToRestaurant: function() {
            var sa = this.model.get('fromServiceAccommodatorId');
            var sl = this.model.get('fromServiceLocationId');
            Vent.trigger('viewChange', 'chat', [sa, sl]);
        },

        render: function() {
            var data = _.extend(this.model.toJSON());
            this.$el.html(this.template(data));
            return this;
        }
    });

    return MymessageItemView;

});
