/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'Vent',
    'templates',
    'loader',
    'actions/saslActions'
], function ($, _, Backbone, Vent, JST, loader, saslActions) {
    'use strict';

    var AboutUsButton = Backbone.View.extend({

        template: JST['app/scripts/templates/partials/aboutUsButton.ejs'],

        events: {
            'click': 'triggerAboutUsView'
        },

        initialize: function (options) {
            options = options || {};
            this.parent = options.parent;

            this.listenTo(this.parent, 'hide', this.remove, this);
        },

        render: function () {
            this.$el.html(this.template());
            return this;
        },

        triggerAboutUsView: function() {
            Vent.trigger('viewChange', 'aboutUs', this.model.getUrlKey());
        }
    });

    return AboutUsButton;
});
