/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'Vent',
    'viewFactory',
    'loader',
    'views/pageLayout',
    'views/listView',
    'views/partials/editableView'
], function ($, _, Backbone, Vent, viewFactory, loader, PageLayout, ListView, EditableView) {
    'use strict';

    var EditView = PageLayout.extend({

        name: 'edit',

        initialize: function(options) {
            options = options || {};

            this.restaurant = options.restaurant;
            this.user = options.user;
            this.action = options.action;
            this.actionfn = options.actionfn;
            this.items = options.items;

            this.on('show', this.onShow, this);
            this.on('hide', this.onHide, this);

        },

        onShow:  function() {
            this.addEvents({
                'click .back': 'triggerRestaurantView',
            });

            this.listenTo( Vent, 'logout_success', this.triggerRestaurantView, this);
            this.renderItems();
        },

        renderItems: function() {
            this.$('.cmntyex-items_placeholder').html( new ListView({
                ItemView: EditableView,
                ItemViewOptions: {
                    action: this.action,
                    actionfn: this.actionfn,
                },
                className: 'cmntyex-editable_list',
                collection: this.items,
                dataRole: 'none',
                parent: this
            }).render().el);
        },

        triggerRestaurantView: function() {
            Vent.trigger( 'viewChange', 'restaurant', this.restaurant.getUrlKey(), { reverse: true } );
        }

    });

    return EditView;
});
