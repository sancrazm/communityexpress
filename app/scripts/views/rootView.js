/*global define*/

define([
    'views/pageLayout'
], function (PageLayout) {
    'use strict';

    var RootView = PageLayout.extend({
        name: 'root',
    });

    return RootView;
});
