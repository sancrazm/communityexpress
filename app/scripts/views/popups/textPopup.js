/*global define*/

define([
    'jquery',
    'underscore',
    'templates',
    'views/popupView',
], function ($, _, JST, PopupView) {
    'use strict';

    var TextPopup = PopupView.extend({

        template: JST['app/scripts/templates/textPopup.ejs'],

        id: 'cmntyex_text_popup',

        className: 'popup',

        initialize: function (options) {
            this.text = options.text;
            this.options = options || {};
        },

        render: function () {
            this.$el.html(this.template({text: this.text}));
            return this;
        },

        onShow: function () {
            if (this.options.select) {
                this.selectText();
            }
            if (this.options.wordBreak) {
                this.$('.ui-content').css({
                    'word-wrap': 'break-word'
                });
            }
        },

        selectText: function () {
            var el = this.$('.ui-content')[0];
            var range = document.createRange();
            range.selectNodeContents(el);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        }

    });

    return TextPopup;
});
