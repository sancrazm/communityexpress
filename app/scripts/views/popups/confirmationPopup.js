/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'views/popupView'
], function ($, _, Backbone, JST, PopupView) {
    'use strict';

    var ConfirmationPopup = PopupView.extend({
        template: JST['app/scripts/templates/confirmationPopup.ejs'],

        initialize: function(options){
            this.text = options.text;
            this.action = options.action;

            this.addEvents({
                'click .confirmation_button': 'performAction'
            });

            this.renderData = {
                text: this.options.text
            };
        },

        performAction: function() {
            this.shut();
            this.$el.on('popupafterclose', function () {
                this.action();
            }.bind(this));
        }

    });

    return ConfirmationPopup;
});
