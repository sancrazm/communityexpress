/*global define*/

define([
    'jquery',
    'underscore',
    'templates',
    'loader',
    'views/popupView',
    'actions/contactActions'
], function ($, _, JST, loader, PopupView, contactActions) {
    'use strict';

    var contactPopup = PopupView.extend({

        template: JST['app/scripts/templates/contactPopup.ejs'],

        id: 'cmntyex_contact_popup',

        className: 'popup',

        initialize: function (options) {
            options = options || {};
            this.promoUUID = options.promoUUID;
            this.sasl = options.sasl;

            this.addEvents({
                'submit': 'onSubmitClick'
            });

            this.$el.attr('data-dismissible','false');
        },

        render: function () {
            this.$el.html(this.template({name: 'mobile'}));
            return this;
        },

        onSubmitClick: function (e) {
            e.preventDefault();
            var contact = this.$('input[name=contact]').val();
            if (contact) {
                loader.show();
                contactActions.sendPromoURLToMobile(
                    this.sasl.sa(),
                    this.sasl.sl(),
                    contact,
                    this.promoUUID
                ).then(function () {
                    loader.showFlashMessage('SMS sent');
                    this.shut();
                }.bind(this), function (err) {
                    loader.showErrorMessage(err, 'Error sending SMS');
                });
            }
        }
    });

    return contactPopup;
});
