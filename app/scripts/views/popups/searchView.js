/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'Vent',
    'loader',
    'appConfig',
    'views/popupView',
    'actions/saslActions',
    'globalHelpers'
], function ($, _, Backbone, JST, Vent, loader, config, PopupView, saslActions, h) {
    'use strict';

    var SearchView = PopupView.extend({

        template: JST['app/scripts/templates/search.ejs'],

        id: 'search_popup',

        initialize: function(){
            this.addEvents({
                'submit': 'submitForm'
            });
        },

        submitForm: function() {
            var data = this.getFormData();
            loader.show();
            saslActions.getSaslSummaryByAddress(data.city, data.street, data.zip)
                    .then(this._onSuccess.bind(this), this._onError.bind(this));

            return false;
        },

        getFormData: function() {
            var values = {};
            $.each(this.$el.find('form').serializeArray(), function(i, field) {
                values[field.name] = field.value;
            });
            return values;
        },

        _onSuccess: function(response) {
            this.shut();
            loader.hide();
        },

        _onError: function(e) {
            loader.showFlashMessage(h().getErrorMessage(e, 'Error searching by address'));
        }

    });

    return SearchView;
});
