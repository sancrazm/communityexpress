/*global define*/

define([
    'jquery',
    'underscore',
    'templates',
    'views/popupView',
], function ($, _, JST, PopupView) {
    'use strict';

    var memberPassword = PopupView.extend({

        template: JST['app/scripts/templates/memberPassword.ejs'],

        id: 'cmntyex_member_password',

        className: 'popup',

        initialize: function (options) {
            options = options || {};
            this.onSubmit = options.onSubmit;
            this.onCancel = options.onCancel;

            this.addEvents({
                'click .cancel_button': 'onCancelClick',
                'submit': 'onSubmitClick'
            });

            this.$el.attr('data-dismissible','false');
        },

        render: function () {
            this.$el.html(this.template({}));
            return this;
        },

        onSubmitClick: function (e) {
            e.preventDefault();
            var password = this.$('input[name=password]').val();
            if (password) {
                this.onSubmit(password);
                this.shut();
            }
        },

        onCancelClick: function () {
            this.onCancel();
            this.shut();
        }

    });

    return memberPassword;
});
