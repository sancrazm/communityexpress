/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'views/popupView'
], function ($, _, Backbone, JST, PopupView) {
    'use strict';

    var SigninalertView = PopupView.extend({
        template: JST['app/scripts/templates/signinalert.ejs'],

        initialize: function(){
            this.$el.attr({
                'id': 'cmntyex_signinalert_popup',
            });
            this.addEvents({
                'click .signinalert_button': 'goToSignIn'
            });
        },

        goToSignIn: function() {
            this.shut();
            this.$el.on('popupafterclose', function () {
                this.parent.openSubview('signin');
            }.bind(this));
        }

    });

    return SigninalertView;
});
