/*global define*/

define([
    'jquery',
    'underscore',
    'views/popupView',
    'templates',
    'maps/Map',
    'loader',
    'actions/saslActions'
], function ($, _, PopupView, JST, Map, loader, saslActions) {
    'use strict';

    var UploadView = PopupView.extend({

        template: JST['app/scripts/templates/updateLocation.ejs'],

        id: 'cmntyex_update_location_popup',

        className: 'popup',

        initialize: function (options) {
            this.addEvents({
                'click .confirmation_button':'updateLocation',
                'click .update_button': 'toggleConfirmation',
                'click .cancel_confirmation':'toggleConfirmation'
            });

        },

        beforeShow: function () {
            var h = $( window ).height();
            var w = $( window ).width();
            this.$el.css({
                'width': '300px',
            });
            this.map = new Map(this.$('.cmntyx-map_placeholder'));
        },

        toggleConfirmation: function () {
            this.$('.cmntyx-update_location_confirm').toggle();
            this.$('.cmntyx-update_location_form').toggle();
        },

        updateLocation: function () {
            var status = this.$('select[name=select-status]').val();
            var coords = this.map.getCenter();
            loader.show('updating status');
            saslActions.updateLocation(this.model.sa(), this.model.sl(), coords.lat, coords.lng, status)
                .then(function () {
                    loader.showFlashMessage('status updated');
                    this.shut();
                }.bind(this), function () {
                    loader.showFlashMessage('Error updating location');
                });
        },

    });

    return UploadView;
});
