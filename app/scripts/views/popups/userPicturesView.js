/*global define*/

define([
    'jquery',
    'underscore',
    'templates',
    'loader',
    'views/popupView',
    'swipe'
], function ($, _, JST, loader, PopupView) {
    'use strict';

    var PromotionsView = PopupView.extend({

        template: JST['app/scripts/templates/userPictures.ejs'],

        id: 'cmntyex_user_pictures_popup',

        className: 'popup',

        initialize: function() {
            this.addEvents({
                'click .close_button': 'shut',
            });
        },

        beforeShow: function(){
            var w = $(window).width();
            this.$el.css({
                'max-width': 400,
                'width': w * 0.85
            });
        },

        onShow: function(){
            this.init();
        },

        init: function() {
            this.initGallery();
        },

        initGallery: function(){
            this.gallery = this.$el.find('.slider').Swipe({
                callback: function(index) {
                    this.loadImage(index);
                    this._toggleButtons(index);
                }.bind(this)
            }).data('Swipe');
        },

        goToSlide: function(index) {
            this.gallery.slide(index);
        }

    });

    return PromotionsView;
});
