/*global define*/


define([
    'jquery',
    'underscore',
    'templates',
    'views/panelView',
    'appConfig'
], function ($, _, JST, PanelView, config ) {
    'use strict';

    var defaults = {
        imagePath: config.imagePath
    };

    var RestaurantMenuView = PanelView.extend({

        template: JST['app/scripts/templates/restaurantMenu.ejs'],

        initialize: function(options){
            this.options = options || {};
            this.$el.attr('id', 'cmntyex_menu_panel');
            this.menuOptions = _.extend({}, defaults, options);
        },

        render: function() {
            var buttons = _.filter(this.options, function (option, key) {
                if (!option || !option.masterEnabled) return false;
                option.key = key;
                return true;
            });
            this.$el.html(this.template({buttons: buttons}));
            return this;
        }
    });

    return RestaurantMenuView;
});
