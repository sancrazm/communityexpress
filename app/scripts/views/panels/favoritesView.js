/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'Vent',
    'globalHelpers',
    'views/panelView',
    'views/partials/favorite_item'
], function ($, _, Backbone, JST, Vent, h, PanelView, FavoriteView) {
    'use strict';

    var FavoritesView = PanelView.extend({

        template: JST['app/scripts/templates/favorites.ejs'],

        initialize: function(){
            this.$el.attr({
                'id': 'cmntyex_favorites_panel',
            });
        },

        render: function() {
            var self = this;
            this.$el.html(this.template());

            if(this.collection.length < 1){
                this.$el.find('.no_items_message').show();
            }

            var frg = document.createDocumentFragment();
            this.collection.each(function(model){
                frg.appendChild(self.renderItem(model));
            });
            this.$el.find('ul').append(frg);
            return this;
        },

        renderItem: function(item) {
            var li = new FavoriteView({ model: item });
            return li.render().el;
        }

    });

    return FavoritesView;
});
