/*global define*/

define([
    'jquery',
    'underscore',
    'views/panelView',
    'templates',
    'loader',
    'actions/favoriteActions',
    'globalHelpers'
], function ($, _, PanelView, JST, loader, favoriteActions, h) {
    'use strict';

    var ShareView = PanelView.extend({

        template: JST['app/scripts/templates/shareView.ejs'],

        initialize: function () {
            this.addEvents({
                'click .add_to_favorites_button' : 'addToFavorites',
                'click .copy_url_button' : 'copyUrl'
            });
        },

        renderData: {
            url: window.location
        },

        addToFavorites: function () {

            this.parent.withLogIn(function () {


                if( this._isInFavorites() ){
                    loader.showFlashMessage(this.model.get('saslName') + ' is already in favorites');
                    return;
                }

                loader.show();
                favoriteActions.addFavorite( this.model.sa(), this.model.sl() )
                    .then(this._onAddFavorite.bind(this), this._onAddFavoriteError.bind(this));
            }.bind(this));
        },

        _isInFavorites: function() {
            return this.user.favorites.get( this.model.getSasl() );
        },

        _onAddFavorite: function () {
            loader.showFlashMessage('Added ' + this.model.get('saslName') + ' to favorites');
        },

        _onAddFavoriteError: function (e) {
            loader.showFlashMessage(h().getErrorMessage(e, 'Error adding favorite'));
        }

    });

    return ShareView;
});
