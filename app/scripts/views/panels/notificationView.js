/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'views/panelView',
    'views/partials/notificationView',
    'views/listView',
    'actions/communicationActions'
], function ($, _, Backbone, JST, PanelView, NotificationView, ListView, communicationActions) {
    'use strict';

    var NotificationsView = PanelView.extend({

        template: JST['app/scripts/templates/notification.ejs'],

        id: 'cmntyex_notification_panel',

        initialize: function(options){
            options = options || {};

            this.addEvents({
                'click .close-panel-button': 'shut',
                'panelopen': 'markAsRead'
            });
        },

        render: function() {
            this.$el.html(this.template());
            this.$('.cmntyex-list_container').append(new ListView({
                collection: this.collection,
                update: false,
                ItemView: NotificationView,
                parent: this
            }).render().el);
            return this;
        },

        markAsRead: function() {
            this.collection.each(function (notification) {
                communicationActions.markAsRead(notification.get('communicationId'), notification.get('offset'));
            }.bind(this));
        }

    });

    return NotificationsView;
});
