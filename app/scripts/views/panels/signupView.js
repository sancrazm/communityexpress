/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'Vent',
    'loader',
    'appConfig',
    'views/popupView',
    'actions/sessionActions',
    'globalHelpers'
], function ($, _, Backbone, JST, Vent, loader, config, PopupView, sessionActions, h) {
    'use strict';

    var SignupView = PopupView.extend({

        template: JST['app/scripts/templates/signup.ejs'],

        id: 'signup_panel',

        initialize: function(){
            this.addEvents({
                'submit': 'submitForm'
            });
        },

        submitForm: function(e) {
            e.preventDefault();
            var data = this.getFormData();

            loader.show();
            sessionActions.registerNewMember(
                this.model.sa(),
                this.model.sl(),
                data.username,
                data.password,
                data.email,
                data.password_confirmation)
                    .then(this._onSignupSuccess.bind(this), this._onSignupError.bind(this));

            return false;
        },

        getFormData: function() {
            var values = {};
            $.each(this.$el.find('form').serializeArray(), function(i, field) {
                values[field.name] = field.value;
            });
            return values;
        },

        _onSignupSuccess: function(response) {
            loader.showFlashMessage( 'successfully signed up as ' + response.username );
            this.shut();
        },

        _onSignupError: function(e) {
            if(e && e.type === 'validation'){
                loader.showFlashMessage( e.message );
            } else {
                loader.showFlashMessage(h().getErrorMessage(e, 'Error signin up'));
            }
        }

    });

    return SignupView;
});
