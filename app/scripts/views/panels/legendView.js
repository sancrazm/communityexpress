/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'views/panelView',
], function ($, _, Backbone, JST, PanelView) {
    'use strict';

    var defaults = {
        url: '',
        title: 'Legend',
        markers: []
    };

    var LegendView = PanelView.extend({

        template: JST['app/scripts/templates/legendView.ejs'],

        initialize: function(options) {
            options = options || {};
            this.legendData = _.extend({}, defaults, options);
            this.$el.attr('id', 'cmntyex_legend_panel' );
        },

        render: function() {
            this.$el.html(this.template(this.legendData));
            return this;
        }

    });

    return LegendView;
});
