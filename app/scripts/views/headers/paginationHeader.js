/*global define*/

define([
    'underscore',
    'jquery',
    'backbone',
    'templates',
], function (_,$, Backbone, JST) {
    'use strict';

    var PaginationHeader = Backbone.View.extend({

        template: JST['app/scripts/templates/toolbars/pagination_header.ejs'],

        initialize: function(options) {
            this.options = options || {};
            this.page = options.page;
        },

        render: function() {
            this.$el.append(this.template(_.extend(this.options.sasl.attributes, this.options)));
            this.setElement(this.$el.children().eq(0));
            this.reinitialize();
            return this;
        },

        reinitialize: function(){
            this.$el.toolbar();
        }

    });

    return PaginationHeader;
});
