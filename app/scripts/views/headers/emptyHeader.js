/*global define*/

define([
    'underscore',
    'jquery',
    'backbone',
    'templates',
], function (_,$, Backbone, JST) {
    'use strict';

    var EmptyHeader = Backbone.View.extend({

        template: JST['app/scripts/templates/toolbars/empty_header.ejs'],

        render: function() {
            this.$el.html(this.template());
            this.setElement(this.$el.children().eq(0));
            this.reinitialize();
            return this;
        },

        reinitialize: function(){
            this.$el.toolbar();
        }

    });

    return EmptyHeader;
});
