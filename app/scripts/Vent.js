/*global define*/

'use strict';

var Backbone = require ('backbone');

var vent = _.extend({},Backbone.Events);

module.exports = vent;
