<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="description" content="">
<meta name="keywords" content="">
<title>Sitelettes</title>

<link href="/no_sitelette/bootstrap.min.css" rel="stylesheet">
<link href="/no_sitelette/style.css" rel="stylesheet">

<script src="/no_sitelette/jquery.js"></script>
<script src="/no_sitelette/bootstrap.min.js"></script>
<script src="/no_sitelette/script.js"></script>
 
</head>
<body>
   Get your free sitelette at <a href="https://sitelettes.com">sitelettes.com</a>
</body>
</html>
